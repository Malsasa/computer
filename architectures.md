# Architectures of Computers

Computers differ because of the most basic things to them, their architectures. An architecture is a design in which the computer processor hardware created upon it. In most basic saying, an architecture is what your computer's CPU belongs to a group of computers where your software can run on. There are a few of computer architectures and the best example is your own desktop PC or laptop which belongs to X86 architecture. Other example will be your own smartphone which belongs to 

## Personal Computer 

Personal Computer (PC) or technically known as X86 are the most used architecture. It is the architecture of your desktop PC and laptop since 1980's up to today. The company who first invented this architecture was Intel, and the one who first mass produce it as computers was IBM, and the operating systems first used with this type of computers were MS-DOS and Windows. This architecture is divided into two generations, the 32 bit (also known as X86, X86_32, i386, i586, i686), and the 64 bit (also known as X86_64 or amd64). Difference between two generations is that the 64 is generally more advanced in technology than the 32. The initial naming i and amd from i386 and amd64 came from the companies who invented them namely Intel and AMD. Thus, Intel's CPU products have both i386 and amd64 choices, and likewise AMD's CPU products also have both i386 and amd64 choices.

## ARM Computers

ARM or technically known as the embedded computers architecture is the one invented after Personal Computer with the feature of low-power consumption and smaller form size. It is the architecture of your smartphone and small board computer (e.g. Raspberry Pi) today. Most people who use smartphones today are ARM users. Thus, a smartphone is a computer because of its architecture. This architecture is divided into two generations, the 32 bit (also known as ARM32, armhf, armv7), and the 64 bit (also known as ARM64, armv8). Companies who create ARM processors are Atmel, Mali, and Texas Instruments. Companies who sell ARM computers are Apple, Asus, Lenovo, Samsung, Oppo, Xiaomi, Vivo.

## Other Architectures

There are other architectures such as PowerPC, MIPS, and s390. There is possibility new architectures may be invented in the future. 

## Relations to Software

Architecture rules software. Your computer architecture rules your computer software. Your software cannot be different to your architecture. If your computer is x86, your software must be x86. If your computer is x86_64, your software must be x86_64 as well. Likewise, if your device is ARM, your software for that device must be ARM too. This rule applies for other computer architectures. This means for a computer user, he cannot run a software unless it is matched with the architecture of his computer. For example, if there is a word processing software, libreoffice-writer-x86_64.app, the user cannot run this software on desktop, laptop, server, or smartphone computer unless his computer is also x86_64.

## 32 bit and 64 bit

Architectures are developed based on mathematics of 2 power n. This means the set of numbers we know are 2, 4, 8, 16, 32, 64, 128 ... Because of that, the technology is developed firstly in the ancient time 8 bit, mathematically 2 power 8, and next people invented 16 bit, mathematically 2 power 16, and then people invented 32 bit, mathematically 2 power 32, and finally people invented 64 bit, mathematically 2 power 64. Today, only two generations of computer architectures generally available which are 32 bit and 64 bit. However, for PC architecture, the 32 bit ones are declining mass production today so we will rarely see computers with 32 bit processor today.