# What is a computer?

Computer is electronic machine to process and store information. 

It processes and stores information in two forms namely data and software. Every information either it is software or data is understood by the computer as electronic signal on and off and this represented as binary numbers one and zero and can be stored in computer storage. Computer's purpose is to do user's commands to process information. To do this purpose, computer has parts with each one has its own functionality such as keyboard for user to command, display for user to see, and hard disk to store information.

# Computer brands

Computers are produced by manufacturers. Brands are names of those manufacturers and often known with certain logos. Several examples of computer brands are Asus, Dell, and Lenovo among others.

# Computer architectures

Computers are designed with certain choices of architectures. They differ essentially because of their architectures and not brands. The most used architecture are called x86 (varies into choices of 32 and 64 bits) which is the architectures of desktop and laptop computers nowadays. The second most used architectures are called ARM (also varies into choices of 32 and 64 bits) which is the architecture of mobile phones and embedded computers. Beyond these two, there are still several others such as MIPS and SPARC among the rest. Why this knowledge is important? Because computer architecture limits software applications and operating systems you can use. For example software application for ARM can only run on ARM computers, and similarly operating systems for x86 can only run on x86 computers as well -- otherwise those programs cannot be used. 