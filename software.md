# Computer Software

Software in English means ware (tool) that is soft. Software is a part of computer which cannot be touched (soft) as an opposite to hardware which can be touched (hard). Software is also known as program, code, listing, tool, utility, or application. Software is an untouchable, active and doing part of computer, which can be added or removed, created by writing codes in a certain language, which is used by the user to do a job. Software has attributes of active and doing as a difference to data which has attributes of passive and being done by. Generally it is easier to create software than to create hardware and as a result there are much more software available than hardware. Example of software are word processor, a program to type documents and print them, audio player, a program to play music and such files, and web browser, a program to read information pages on the internet.

## Terminology

In English, the term software comes from two words, ware and software, to indicate it is something like tool with the attribute of soft. Soft here means untouchable, to differ it with hardware, which hard in it means touchable.

Synonyms to software are programs, code, listing, tool, utility, or application. In the past, all software were called programs. In modern days, as of today, all software are called applications, or abbriviated, apps. 

Antonym to software is hardware. 

## Software Creation

Software are written. In this meaning, software is the same as books, poems, and mathemathics. Software must be written in a certain rules which are called programming language. Hence, the activity of writing software, creating it, is also called programming. A written software requires to be translated to  machine language, which differs following the computer architectures, so that the software can run on that computer or be used for the user. If a written software has not been translated to machine language, the user cannot make use of it or the computer cannot run it. This all means in computing generally, and software creation particularly, there are 3 or 5 items involved which are the software, the author, the language, the translator tool, the computer, and the user. To learn more about software creation, read Software Development. 

See Also

Categories of Software
How Software Created
Relations between hardware and software
