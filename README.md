# Learn Computer

Your book to start learning computer from the basics.

# What is a Computer

[Computer Basics](https://codeberg.org/Malsasa/computer/src/branch/main/computer.md)

Desktop, laptop, and mobile computers 

[Asus, Lenovo, and other brands](https://codeberg.org/Malsasa/computer/src/branch/main/computer.md#computer-architectures)

[Architectures](https://codeberg.org/Malsasa/computer/src/branch/main/architectures.md)

# Understanding Hardware

Hardware Basics

Keyboard, mouse, and input devices

CPU, GPU, and processing devices 

Display, speaker, and output devices

Buttons

Printer and other peripherals

# Understanding Software

[Software Basics](https://codeberg.org/Malsasa/computer/src/branch/main/software.md)


# Parts of a Computer

# Operating systems

# Applications

# Using a Computer

